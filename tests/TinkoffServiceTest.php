<?php


use GuzzleHttp\Client;
use GuzzleHttp\Psr7\HttpFactory;
use Uplinestudio\TinkoffPaymentSdk\TinkoffCredentials;
use Uplinestudio\TinkoffPaymentSdk\TinkoffService;
use PHPUnit\Framework\TestCase;

class TinkoffServiceTest extends TestCase
{

    public function testParseNotification()
    {
        $notificatipon = (new TinkoffService(
            new Client(),
            new HttpFactory(),
            new HttpFactory(),
            new TinkoffCredentials(
                '1321054611234DEMO',
                'Dfsfh56dgKl'
            )
        ))->parseNotification([
            "TerminalKey" => "1321054611234DEMO",
            "OrderId" => "201709",
            "Success" => true,
            "Status" => "AUTHORIZED",
            "PaymentId" => 8742591,
            "ErrorCode" => "0",
            "Amount" => 9855,
            "CardId" => 322264,
            "Pan" => "430000******0777",
            "ExpDate" => "1122",
            "RebillId" => '101709',
            "Token" => "b906d28e76c6428e37b25fcf86c0adc52c63d503013fdd632e300593d165766b"
        ]);

        $this->assertEquals($notificatipon->getTerminalKey(), '1321054611234DEMO');
        $this->assertEquals($notificatipon->getOrderId(), '201709');
        $this->assertEquals($notificatipon->getSuccess(), true);
        $this->assertEquals($notificatipon->getStatus(), 'AUTHORIZED');
        $this->assertEquals($notificatipon->getPaymentId(), 8742591);
        $this->assertEquals($notificatipon->getErrorCode(), '0');
        $this->assertEquals($notificatipon->getAmount(), 9855);
        $this->assertEquals($notificatipon->getCardId(), 322264);
        $this->assertEquals($notificatipon->getPan(), '430000******0777');
        $this->assertEquals($notificatipon->getExpDate(), '1122');
        $this->assertEquals($notificatipon->getRebillId(), '101709');
        $this->assertEquals($notificatipon->getToken(), 'b906d28e76c6428e37b25fcf86c0adc52c63d503013fdd632e300593d165766b');
    }
}
