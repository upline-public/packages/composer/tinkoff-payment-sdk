<?php

namespace Uplinestudio\TinkoffPaymentSdk\Responses\Data;

use MyCLabs\Enum\Enum;

final class Status extends Enum
{
    private const CREATED = 'NEW'; // Создан
    private const FORM_SHOWED = 'FORM_SHOWED'; // Платежная форма открыта покупателем
    private const DEADLINE_EXPIRED = 'DEADLINE_EXPIRED'; // Просрочен
    private const CANCELED = 'CANCELED'; // Отменен
    private const PREAUTHORIZING = 'PREAUTHORIZING'; // Проверка платежных данных
    private const AUTHORIZING = 'AUTHORIZING'; // Резервируется
    private const AUTHORIZED = 'AUTHORIZED'; // Зарезервирован
    private const AUTH_FAIL = 'AUTH_FAIL'; // Не прошел авторизацию
    private const REJECTED = 'REJECTED'; // Отклонен
    private const REVERSING = 'REVERSING'; // Резервирование отменяется
    private const PARTIAL_REVERSED = 'PARTIAL_REVERSED'; // Резервирование отменено частично
    private const REVERSED = 'REVERSED'; // Резервирование отменено
    private const CONFIRMING = 'CONFIRMING'; // Подтверждается
    private const CONFIRMED = 'CONFIRMED'; // Подтвержден
    private const REFUNDING = 'REFUNDING'; // Возвращается
    private const PARTIAL_REFUNDED = 'PARTIAL_REFUNDED'; // Возвращен частично
    private const REFUNDED = 'REFUNDED'; // Возвращен полностью

    private const THREE_DS_CHECKING = '3DS_CHECKING';  // Проверяется по протоколу 3-D Secure
    private const THREE_DS_CHECKED = '3DS_CHECKED';  // Проверен по протоколу 3-D Secure
}





