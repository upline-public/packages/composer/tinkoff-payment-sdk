<?php

namespace Uplinestudio\TinkoffPaymentSdk\Responses;

class ErrorResponse
{
    private int $ErrorCode; //	Код ошибки
    private ?string $Message = null;
    private ?string $Details = null;

    public function __construct(array $data)
    {

        $this->ErrorCode = $data['ErrorCode'];
        if (isset($data['Message'])) {
            $this->Message = $data['Message'];
        }
        if (isset($data['Details'])) {
            $this->Details = $data['Details'];
        }
    }

    /**
     * @return int
     */
    public function getErrorCode(): int
    {
        return $this->ErrorCode;
    }

    /**
     * @return string|null
     */
    public function getMessage(): ?string
    {
        return $this->Message;
    }

    /**
     * @return string|null
     */
    public function getDetails(): ?string
    {
        return $this->Details;
    }
}
