<?php

namespace Uplinestudio\TinkoffPaymentSdk\Responses;

use Uplinestudio\TinkoffPaymentSdk\Responses\Data\Status;

class InitResponse
{
    private string $TerminalKey; //	Идентификатор терминала. Выдается продавцу банком при заведении терминала
    private int $Amount; //	Сумма в копейках
    private string $OrderId; //	Идентификатор заказа в системе продавца
    private ?Status $Status = null; //	Статус платежа	string(20)	Нет
    private int $PaymentId; //	Идентификатор платежа в системе банка
    private ?string $PaymentURL = null;

    public function __construct(array $data)
    {
        $this->TerminalKey = $data['TerminalKey'];
        $this->Amount = $data['Amount'];
        $this->OrderId = $data['OrderId'];
        if (isset($data['Status'])) {
            $this->Status = Status::from($data['Status']);
        }
        $this->PaymentId = $data['PaymentId'];
        if (isset($data['PaymentURL'])) {
            $this->PaymentURL = $data['PaymentURL'];
        }
    }

    /**
     * @return string
     */
    public function getTerminalKey(): string
    {
        return $this->TerminalKey;
    }

    /**
     * @return int
     */
    public function getAmount(): int
    {
        return $this->Amount;
    }

    /**
     * @return string
     */
    public function getOrderId(): string
    {
        return $this->OrderId;
    }

    /**
     * @return Status|null
     */
    public function getStatus(): ?Status
    {
        return $this->Status;
    }

    /**
     * @return int
     */
    public function getPaymentId(): int
    {
        return $this->PaymentId;
    }

    /**
     * @return string|null
     */
    public function getPaymentURL(): ?string
    {
        return $this->PaymentURL;
    }
}
