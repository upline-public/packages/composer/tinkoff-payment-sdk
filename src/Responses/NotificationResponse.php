<?php

namespace Uplinestudio\TinkoffPaymentSdk\Responses;

class NotificationResponse
{
    private array $fullData;
    private string $terminalKey;
    private string $orderId;
    private bool $success;
    private string $status;
    private string $paymentId;
    private string $errorCode;
    private int $amount;
    private ?string $rebillId;
    private ?int $cardId;
    private ?string $pan;
    private ?string $expDate;
    private string $token;

    public function __construct(array $data)
    {
        $this->fullData = $data;
        $this->terminalKey = $data['TerminalKey'];
        $this->orderId = $data['OrderId'];
        $this->success = $data['Success'];
        $this->status = $data['Status'];
        $this->paymentId = $data['PaymentId'];
        $this->errorCode = $data['ErrorCode'];
        $this->amount = $data['Amount'];
        $this->rebillId = $data['RebillId'] ?? null;
        $this->cardId = $data['CardId'] ?? null;
        $this->pan = $data['Pan'] ?? null;
        $this->expDate = $data['ExpDate'] ?? null;
        $this->token = $data['Token'] ?? null;
    }

    public function getFullData(): array
    {
        return $this->fullData;
    }

    public function getTerminalKey(): string
    {
        return $this->terminalKey;
    }

    public function getOrderId(): string
    {
        return $this->orderId;
    }

    public function getSuccess(): bool
    {
        return $this->success;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function getPaymentId(): string
    {
        return $this->paymentId;
    }

    public function getErrorCode(): string
    {
        return $this->errorCode;
    }

    public function getAmount(): int
    {
        return $this->amount;
    }

    public function getRebillId(): ?string
    {
        return $this->rebillId;
    }

    public function getCardId(): ?int
    {
        return $this->cardId;
    }

    public function getPan(): ?string
    {
        return $this->pan;
    }

    public function getExpDate(): ?string
    {
        return $this->expDate;
    }

    public function getToken(): ?string
    {
        return $this->token;
    }

}
