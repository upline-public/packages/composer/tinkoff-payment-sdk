<?php

namespace Uplinestudio\TinkoffPaymentSdk\Requests;

use Uplinestudio\TinkoffPaymentSdk\Utils\Arrayable;

interface ApiRequest extends Arrayable
{
    public function getUrl(): string;
}
