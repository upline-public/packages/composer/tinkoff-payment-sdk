<?php

namespace Uplinestudio\TinkoffPaymentSdk\Requests\Data;

use MyCLabs\Enum\Enum;

/**
 * Система налогообложения
 * @method static Taxation OSN общая
 * @method static Taxation USN_INCOME упрощенная (доходы)
 * @method static Taxation USN_INCOME_OUTCOME упрощенная (доходы минус расходы)
 * @method static Taxation PATENT патентная
 * @method static Taxation ENVD единый налог на вмененный доход
 * @method static Taxation ESN единый сельскохозяйственный налог
 */
final class Taxation extends Enum
{
    private const OSN = 'osn';
    private const USN_INCOME = 'usn_income';
    private const USN_INCOME_OUTCOME = 'usn_income_outcome';
    private const PATENT = 'patent';
    private const ENVD = 'envd';
    private const ESN = 'esn';

}
