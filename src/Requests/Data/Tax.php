<?php

namespace Uplinestudio\TinkoffPaymentSdk\Requests\Data;

use MyCLabs\Enum\Enum;

/**
 * Система налогообложения
 * @method static TAX NONE() без НДС
 * @method static TAX VAT0() 0%
 * @method static TAX VAT10() 10%
 * @method static TAX VAT20() 20%
 * @method static TAX VAT110() 10/110
 * @method static TAX VAT120() 20/120
 */
final class Tax extends Enum
{
    private const NONE = 'none';
    private const VAT0 = 'vat0';
    private const VAT10 = 'vat10';
    private const VAT20 = 'vat20';
    private const VAT110 = 'vat110';
    private const VAT120 = 'vat120';

}
