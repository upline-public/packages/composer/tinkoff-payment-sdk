<?php

namespace Uplinestudio\TinkoffPaymentSdk\Requests\Data;

use Uplinestudio\TinkoffPaymentSdk\Utils\Arrayable;

class Receipt implements Arrayable
{
    private Taxation $taxation;
    private array $items;
    private ?string $phone = null;
    private ?string $email = null;
    private ?string $emailCompany = null;

    /**
     * @param  Taxation  $taxation
     * @param  ReceiptItem[]  $items
     */
    public function __construct(Taxation $taxation, array $items)
    {
        $this->taxation = $taxation;
        $this->items = $items;
    }

    public function toArray(): array
    {
        if (!$this->phone && !$this->email) {
            throw new \LogicException('At least one of phone or email should be filled');
        }
        $data = [
            'Taxation' => $this->taxation->getValue(),
            'Items' => array_map(fn(ReceiptItem $item) => $item->toArray(), $this->items)
        ];

        if ($this->phone) {
            $data['Phone'] = $this->phone;
        }
        if ($this->email) {
            $data['Email'] = $this->email;
        }
        if ($this->emailCompany) {
            $data['EmailCompany'] = $this->emailCompany;
        }
        return $data;
    }

    /**
     * @param  string|null  $phone
     * @return Receipt
     */
    public function setPhone(?string $phone): Receipt
    {
        $this->phone = $phone;
        return $this;
    }

    /**
     * @param  string|null  $email
     * @return Receipt
     */
    public function setEmail(?string $email): Receipt
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @param  string|null  $emailCompany
     * @return Receipt
     */
    public function setEmailCompany(?string $emailCompany): Receipt
    {
        $this->emailCompany = $emailCompany;
        return $this;
    }
}
