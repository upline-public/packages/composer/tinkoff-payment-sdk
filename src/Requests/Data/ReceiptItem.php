<?php

namespace Uplinestudio\TinkoffPaymentSdk\Requests\Data;

use Uplinestudio\TinkoffPaymentSdk\Utils\Arrayable;

class ReceiptItem implements Arrayable
{
    private string $name;
    private int $quantity;
    private int $amount;
    private int $price;
    private Tax $tax;

    public function __construct(
        string $name,
        int $price,
        int $quantity,
        int $amount,
        Tax $tax
    )
    {
        $this->name = $name;
        $this->quantity = $quantity;
        $this->amount = $amount;
        $this->price = $price;
        $this->tax = $tax;
    }

    public function toArray(): array
    {
        return [
            'Name' => $this->name,
            'Quantity' => $this->quantity,
            'Amount' => $this->amount,
            'Price' => $this->price,
            'Tax' => $this->tax->getValue()
        ];
    }
}
