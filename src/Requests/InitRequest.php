<?php

namespace Uplinestudio\TinkoffPaymentSdk\Requests;

use Uplinestudio\TinkoffPaymentSdk\Requests\Data\Receipt;
use Uplinestudio\TinkoffPaymentSdk\Utils\Arrayable;

class InitRequest implements ApiRequest
{
    private const URL = 'https://securepay.tinkoff.ru/v2/Init';
    private int $amount;
    private string $orderId;
    private ?Receipt $receipt = null;
    private ?string $ip = null;
    private ?string $description = null;
    private ?string $notificationUrl = null;
    private ?string $successUrl = null;
    private ?string $failUrl = null;

    public function __construct(
        int    $amount,
        string $orderId
    )
    {
        $this->amount = $amount;
        $this->orderId = $orderId;
    }

    public function getUrl(): string
    {
        return self::URL;
    }

    public function toArray(): array
    {
        $result = [
            'Amount' => $this->amount,
            'OrderId' => $this->orderId
        ];

        if ($this->receipt) {
            $result['Receipt'] = $this->receipt->toArray();
        }


        if ($this->ip) {
            $result['IP'] = $this->ip;
        }
        if ($this->description) {
            $result['Description'] = $this->description;
        }
        if ($this->notificationUrl) {
            $result['NotificationUrl'] = $this->notificationUrl;
        }
        if ($this->successUrl) {
            $result['SuccessUrl'] = $this->successUrl;
        }
        if ($this->failUrl) {
            $result['FailUrl'] = $this->failUrl;
        }
        return $result;
    }

    /**
     * @param Receipt $receipt
     * @return InitRequest
     */
    public function setReceipt(Receipt $receipt): InitRequest
    {
        $this->receipt = $receipt;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getIp(): ?string
    {
        return $this->ip;
    }

    /**
     * @param string|null $ip
     * @return InitRequest
     */
    public function setIp(?string $ip): InitRequest
    {
        $this->ip = $ip;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     * @return InitRequest
     */
    public function setDescription(?string $description): InitRequest
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getNotificationUrl(): ?string
    {
        return $this->notificationUrl;
    }

    /**
     * @param string|null $notificationUrl
     * @return InitRequest
     */
    public function setNotificationUrl(?string $notificationUrl): InitRequest
    {
        $this->notificationUrl = $notificationUrl;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSuccessUrl(): ?string
    {
        return $this->successUrl;
    }

    /**
     * @param string|null $successUrl
     * @return InitRequest
     */
    public function setSuccessUrl(?string $successUrl): InitRequest
    {
        $this->successUrl = $successUrl;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getFailUrl(): ?string
    {
        return $this->failUrl;
    }

    /**
     * @param string|null $failUrl
     * @return InitRequest
     */
    public function setFailUrl(?string $failUrl): InitRequest
    {
        $this->failUrl = $failUrl;
        return $this;
    }
}
