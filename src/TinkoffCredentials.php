<?php

namespace Uplinestudio\TinkoffPaymentSdk;

class TinkoffCredentials
{
    private string $terminalKey;
    private ?string $signPassword;

    public function __construct(string $terminalKey, ?string $signPassword = null)
    {
        $this->terminalKey = $terminalKey;
        $this->signPassword = $signPassword;
    }

    public function getTerminalKey(): string
    {
        return $this->terminalKey;
    }

    public function getSignPassword(): ?string
    {
        return $this->signPassword;
    }

    public function hasSign(): bool
    {
        return !!$this->signPassword;
    }
}
