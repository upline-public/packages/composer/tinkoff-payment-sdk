<?php

namespace Uplinestudio\TinkoffPaymentSdk\Utils;

interface Arrayable
{
    public function toArray(): array;
}
