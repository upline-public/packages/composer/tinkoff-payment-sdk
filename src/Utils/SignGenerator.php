<?php

namespace Uplinestudio\TinkoffPaymentSdk\Utils;

class SignGenerator
{
    private string $password;
    private const IGNORING_FIELDS = ['Shops', 'Receipt', 'DATA', 'Token'];

    public function __construct(string $password)
    {
        $this->password = $password;
    }

    public function generateToken(array $data): string
    {
        if (isset($data['Success'])) {
            $data['Success'] = $data['Success'] ? 'true' : 'false';
        }
        $fields = array_filter($data, fn($key) => !in_array($key, self::IGNORING_FIELDS), ARRAY_FILTER_USE_KEY);
        $fields['Password'] = $this->password;
        ksort($fields);
        $concatenatedValues = implode('', array_values($fields));
        return hash('sha256', $concatenatedValues);
    }
}
