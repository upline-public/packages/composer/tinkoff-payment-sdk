<?php

namespace Uplinestudio\TinkoffPaymentSdk;

use InvalidArgumentException;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestFactoryInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamFactoryInterface;
use Throwable;
use Uplinestudio\TinkoffPaymentSdk\Exceptions\ApiException;
use Uplinestudio\TinkoffPaymentSdk\Requests\ApiRequest;
use Uplinestudio\TinkoffPaymentSdk\Requests\InitRequest;
use Uplinestudio\TinkoffPaymentSdk\Responses\ErrorResponse;
use Uplinestudio\TinkoffPaymentSdk\Responses\InitResponse;
use Uplinestudio\TinkoffPaymentSdk\Responses\NotificationResponse;
use Uplinestudio\TinkoffPaymentSdk\Utils\SignGenerator;

class TinkoffService
{
    private ClientInterface $httpClient;
    private RequestFactoryInterface $requestFactory;
    private StreamFactoryInterface $streamFactory;
    private TinkoffCredentials $tinkoffCredentials;
    private ?SignGenerator $signGenerator = null;

    public function __construct(
        ClientInterface         $httpClient,
        RequestFactoryInterface $requestFactory,
        StreamFactoryInterface  $streamFactory,
        TinkoffCredentials      $tinkoffCredentials
    )
    {
        $this->httpClient = $httpClient;
        $this->requestFactory = $requestFactory;
        $this->streamFactory = $streamFactory;
        $this->tinkoffCredentials = $tinkoffCredentials;
        if ($tinkoffCredentials->hasSign()) {
            $this->signGenerator = new SignGenerator($tinkoffCredentials->getSignPassword());
        }
    }

    /**
     * @param InitRequest $request
     * @return InitResponse|ErrorResponse
     */
    public function init(InitRequest $request)
    {
        $response = $this->sendRequest($request);
        $data = $this->decodeResponse($response);
        return $this->isResponseSuccess($data) ? new InitResponse($data) : new ErrorResponse($data);
    }

    /**
     * @throws ApiException
     */
    public function parseNotification(array $data): NotificationResponse
    {
        try {
            $notification = new NotificationResponse($data);
        } catch (Throwable $exception) {
            throw new ApiException('Could not parse notification', ApiException::NOTIFICATION_NOT_PARSED, $exception);
        }
        if ($this->signGenerator->generateToken($data) !== $notification->getToken()) {
            throw new ApiException('Token mismatch', ApiException::NOTIFICATION_TOKEN_MISMATCH);
        }
        return $notification;
    }

    private function isResponseSuccess(array $data): bool
    {
        return $data['Success'] ?? false;
    }

    private function decodeResponse(ResponseInterface $response): array
    {
        return json_decode($response->getBody()->getContents(), true);
    }

    private function sendRequest(ApiRequest $apiRequest)
    {
        $request = $this->requestFactory
            ->createRequest('POST', $apiRequest->getUrl())
            ->withAddedHeader('Content-type', 'application/json')
            ->withBody(
                $this->streamFactory->createStream(
                    json_encode(
                        $this->createRequestArray($apiRequest)
                    )
                )
            );

        return $this->httpClient->sendRequest($request);
    }

    private function createRequestArray(ApiRequest $request): array
    {
        $data = $request->toArray();
        $data['TerminalKey'] = $this->tinkoffCredentials->getTerminalKey();
        if ($this->signGenerator) {
            $data['Token'] = $this->signGenerator->generateToken($data);
        }
        return $data;
    }
}
