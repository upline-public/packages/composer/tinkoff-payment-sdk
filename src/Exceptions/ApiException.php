<?php

namespace Uplinestudio\TinkoffPaymentSdk\Exceptions;

use Throwable;

class ApiException extends \Exception
{
    public const NOTIFICATION_NOT_PARSED = 1;
    public const NOTIFICATION_TOKEN_MISMATCH = 2;

    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}