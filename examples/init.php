<?php

require_once dirname(__DIR__) . '/vendor/autoload.php';

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\HttpFactory;
use Uplinestudio\TinkoffPaymentSdk\Requests\Data\Receipt;
use Uplinestudio\TinkoffPaymentSdk\Requests\Data\ReceiptItem;
use Uplinestudio\TinkoffPaymentSdk\Requests\Data\Tax;
use Uplinestudio\TinkoffPaymentSdk\Requests\Data\Taxation;
use Uplinestudio\TinkoffPaymentSdk\Requests\InitRequest;
use Uplinestudio\TinkoffPaymentSdk\TinkoffCredentials;
use Uplinestudio\TinkoffPaymentSdk\TinkoffService;

$tinkoffService = new TinkoffService(
    new Client(),
    new HttpFactory(),
    new HttpFactory(),
    new TinkoffCredentials(
        'TinkoffBankTest'
    )
);


$initResponse = $tinkoffService->init(
    (new InitRequest(
        '140000',
        '121050'
    ))->setReceipt(
        (
        new Receipt(
            Taxation::OSN(),
            [
                new ReceiptItem(
                    'Наименование товара 1',
                    10000,
                    1,
                    10000,
                    Tax::VAT10()
                ),
                new ReceiptItem(
                    'Наименование товара 2',
                    20000,
                    2,
                    40000,
                    Tax::VAT20()
                ),
                new ReceiptItem(
                    'Наименование товара 3',
                    30000,
                    3,
                    90000,
                    Tax::VAT10()
                ),
            ]
        )
        )
            ->setEmail('a@test.ru')
            ->setPhone('+79031234567')
            ->setEmailCompany('b@test.ru')
    )
);

var_dump($initResponse);
